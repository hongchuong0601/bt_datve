import { combineReducers } from "redux";
import { btDatVeReducer } from "./BTDatVe/reducer";

export const rootReducer = combineReducers({
  btDatVe: btDatVeReducer,
});
