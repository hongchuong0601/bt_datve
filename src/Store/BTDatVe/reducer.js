import { HANDLE_CHAIR_BOOKINGS, PAY } from "./actionType";

const initialState = {
  chairBookings: [],
  chairBookeds: [],
};

// bõ thay type vs payload cho action để khỏi phải ghi action.type và action.payload
export const btDatVeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case HANDLE_CHAIR_BOOKINGS: {
      const data = [...state.chairBookings];

      // kiểm tra ghế được chọn đã tồn tại trong mảng ghế đang chọn hay không
      const index = data.findIndex((value) => {
        return value.soGhe === payload.soGhe;
      });

      if (index === -1) {
        data.push(payload);
      } else {
        data.splice(index, 1);
      }

      return { ...state, chairBookings: data };
    }

    case PAY: {
      const data = [...state.chairBookeds, ...state.chairBookings];
      return { ...state, chairBookeds: data, chairBookings: [] };
    }

    default:
      return state;
  }
};
