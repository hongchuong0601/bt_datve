import React from "react";
import data from "./data.json";
import ChairList from "./ChairList";
import Result from "./Result";

const BTDatVe = () => {
  return (
    <div className="container mt-5">
      <h1>BTDatVe</h1>
      <div className="row">
        <div className="col-8">
          <h1 className="display-4">Đặt vé xem phim</h1>
          <div className="text-center p-3 font-weight-bold bg-dark text-white mt-3 display-4">
            Screen
          </div>
          {/* danh sách ghế */}
          <ChairList data={data} />
        </div>
        <div className="col-4">
          {/* kết quả đặt vé */}
          <Result />
        </div>
      </div>
    </div>
  );
};

export default BTDatVe;
