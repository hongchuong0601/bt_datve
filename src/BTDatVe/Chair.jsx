import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingAction } from "../Store/BTDatVe/action";
import cn from "classnames";
import "./style.scss";

const Chair = (props) => {
  const { ghe } = props;
  const dispatch = useDispatch();
  const { chairBookings, chairBookeds } = useSelector((state) => {
    return state.btDatVe;
  });
  // console.log("chairBookings:", chairBookings);

  return (
    <button
      className={cn("btn btn-outline-dark font-weight-bold mb-3 Chair", {
        booking: chairBookings.find((e) => e.soGhe === ghe.soGhe),
        booked: chairBookeds.find((e) => e.soGhe === ghe.soGhe),
      })}
      style={{ width: 50 }}
      onClick={() => {
        dispatch(chairBookingAction(ghe));
      }}
    >
      {ghe.soGhe}
    </button>
  );
};

export default Chair;
