import React from "react";
import Chair from "./Chair";

const ChairList = (props) => {
  const { data } = props;
  console.log("data:", data);
  return (
    <div className="mt-5">
      {data.map((hangGhe) => {
        return (
          <div className="d-flex" style={{ gap: 10 }}>
            <p className="" style={{ width: 20 }}>
              {hangGhe.hang}
            </p>
            <div className="d-flex" style={{ gap: 10 }}>
              {hangGhe.danhSachGhe.map((ghe) => {
                return <Chair ghe={ghe} />;
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ChairList;
