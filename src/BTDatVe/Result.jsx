import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingAction, payAction } from "../Store/BTDatVe/action";

const Result = () => {
  const { chairBookings, chairBookeds } = useSelector((state) => state.btDatVe);
  console.log("chairBookeds:", chairBookeds);
  console.log("chairBookings:", chairBookings);
  const dispatch = useDispatch();
  return (
    <div>
      <h1>Danh sách ghế bạn chọn</h1>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark mb-3 Chair booked">
          Ghế đẫ đặt
        </button>
        <button className="btn btn-outline-dark mb-3 Chair booking">
          Ghế đang chọn
        </button>
        <button className="btn btn-outline-dark mb-3">Ghế chưa đắt</button>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>Số ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {chairBookings.map((ghe) => (
            <tr>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia}</td>
              <td>
                <button
                  className="btn btn-warning"
                  onClick={() => {
                    dispatch(chairBookingAction(ghe));
                  }}
                >
                  Hủy
                </button>
              </td>
            </tr>
          ))}

          {/* tính tổng tiền */}
          <tr>
            <td>Tổng Tiền</td>
            <td>
              {chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}
            </td>
          </tr>
        </tbody>
      </table>

      <button
        className="btn btn-success"
        onClick={() => {
          dispatch(payAction());
        }}
      >
        Thanh toán
      </button>
    </div>
  );
};

export default Result;
